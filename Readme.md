# Install msys2 for C and C++ compiler

https://github.com/msys2/msys2-installer/releases/download/2023-05-26/msys2-x86_64-20230526.exe


# In the terminal install mingw-64
```
pacman -S --needed base-devel mingw-w64-ucrt-x86_64-toolchain
```

# Add the path to your MinGW-w64 bin folder to the Windows PATH environment variable by using the following steps:

    * In the Windows search bar, type Settings to open your Windows Settings.
    * Search for Edit environment variables for your account.
    * In your User variables, select the Path variable and then select Edit.
    * Select New and add the MinGW-w64 destination folder you recorded during the installation process to the list. If you selected the default installation steps, the path is: C:\msys64\ucrt64\bin.
    * Select OK to save the updated PATH. For the new PATH to be available, reopen your console windows.

# Complete the code to inject the evil.dll produced in the other directory

```
// location of your evil.dll
char evilDLL[] = "C:\\Users\\user\\Documents\\dllinjector\\DLL-Injector\\evil.dll";
```
find all completeCode inside the code and fill it.

# Compilation of the code
```
g++ hack.cpp -o hack.exe -lws2_32 -s -ffunction-sections -fdata-sections -Wno-write-strings -fno-exceptions -fmerge-all-constants -static-libstdc++ -static-libgcc -fpermissive
```
